import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { setColumn, setRow, setLevel, changeGameState } from '../actions';

const propTypes = {
  level: PropTypes.string.isRequired,
  columns: PropTypes.number.isRequired,
  rows: PropTypes.number.isRequired,
  updateLevel: PropTypes.func.isRequired,
  updateColumn: PropTypes.func.isRequired,
  updateRow: PropTypes.func.isRequired,
  createGame: PropTypes.func.isRequired,
};

const Form = ({
  level,
  columns,
  rows,
  updateLevel,
  updateColumn,
  updateRow,
  createGame,
}) => (
  <Fragment>
    <div>
      <label htmlFor="">Columns:</label>
      <input type="text" value={columns} onChange={updateColumn} />
    </div>
    <div>
      <label htmlFor="">Rows:</label>
      <input type="text" value={rows} onChange={updateRow} />
    </div>
    <div>
      <label htmlFor="">Level:</label>
      <select onChange={updateLevel} value={level}>
        <option value="easy">Easy</option>
        <option value="medium">Medium</option>
        <option value="hard">Hard</option>
      </select>
    </div>
    <button onClick={createGame}>Create Game</button>
  </Fragment>
);

Form.propTypes = propTypes;

const mapStateToProps = state => ({
  level: state.game.level,
  columns: state.game.columns,
  rows: state.game.rows,
});

const mapDipspatchToProps = dispatch => ({
  updateColumn: (e) => {
    dispatch(setColumn(Number(e.target.value)));
  },
  updateRow: (e) => {
    dispatch(setRow(Number(e.target.value)));
  },
  updateLevel: (e) => {
    dispatch(setLevel(e.target.value));
  },
  createGame: () => {
    dispatch(changeGameState('start'));
  },
});

export default connect(
  mapStateToProps,
  mapDipspatchToProps,
)(Form);
