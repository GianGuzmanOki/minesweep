import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Locker from './Locker';

const propTypes = {
  board: PropTypes.arrayOf(PropTypes.array).isRequired,
  columns: PropTypes.number.isRequired,
  minesPlanted: PropTypes.number.isRequired,
  flagsPlanted: PropTypes.number.isRequired,
};

const BigContainer = styled.div`
  width: ${props => props.width};
  float: left;
`;

const Board = ({
  board,
  columns,
  minesPlanted,
  flagsPlanted,
}) => {
  const lockers = [];
  board.forEach((boardRow) => {
    boardRow.forEach((bElem) => {
      lockers.push(<Locker key={bElem.key} bElem={bElem} />);
    });
  });
  return (
    <Fragment>
      <label> Remaining mines: {minesPlanted - flagsPlanted} </label>
      <BigContainer width={`${columns * 40}px`}>
        { lockers }
      </BigContainer>
    </Fragment>
  );
};

Board.propTypes = propTypes;

const mapStateToProps = state => ({
  board: state.game.board,
  columns: state.game.columns,
  minesPlanted: state.game.minesPlanted,
  flagsPlanted: state.game.flagsPlanted,
});

const mapDipspatchToProps = null;

export default connect(
  mapStateToProps,
  mapDipspatchToProps,
)(Board);
