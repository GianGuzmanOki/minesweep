import { SET_LEVEL, SET_COLUMN, SET_ROW, SET_GAME_STATE, OPEN_LOCKER, FLAG_LOCKER, RESET_STATE } from './types';

export const setLevel = value => ({
  type: SET_LEVEL,
  value,
});

export const setColumn = value => ({
  type: SET_COLUMN,
  value,
});

export const setRow = value => ({
  type: SET_ROW,
  value,
});

export const changeGameState = value => ({
  type: SET_GAME_STATE,
  value,
});

export const openLocker = (positionX, positionY) => ({
  type: OPEN_LOCKER,
  positionX,
  positionY,
});

export const flagLocker = (positionX, positionY) => ({
  type: FLAG_LOCKER,
  positionX,
  positionY,
});

export const resetState = () => ({
  type: RESET_STATE,
});
