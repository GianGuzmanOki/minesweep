import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Form from './components/Form';
import Board from './components/Board';
import logo from './logo.svg';
import './App.css';

const propTypes = {
  gameState: PropTypes.string.isRequired,
};

const App = ({
  gameState,
}) => {
  let gameStateDiv;
  switch (gameState) {
    case 'initial':
      gameStateDiv = <div />;
      break;
    case 'start':
      gameStateDiv = <Board />;
      break;
    case 'won':
      gameStateDiv = <label>Great, you won</label>;
      break;
    case 'lost':
      gameStateDiv = <label>Sorry, you lost</label>;
      break;
    default:
      gameStateDiv = '';
  }
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1 className="App-title">My Technical Test</h1>
      </header>
      <Form />
      { gameStateDiv }
    </div>
  );
};


App.propTypes = propTypes;

const mapStateToProps = state => ({
  gameState: state.game.gameState,
});

const mapDipspatchToProps = null;

export default connect(
  mapStateToProps,
  mapDipspatchToProps,
)(App);
