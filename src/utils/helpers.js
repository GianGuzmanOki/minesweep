import _ from 'lodash';

const getRandomPosition = max => Math.floor((Math.random() * 1000) + 1) % max;

export const getMinesAround = (board) => {
  for (let i = 0; i < board.length; i += 1) {
    for (let j = 0; j < board[i].length; j += 1) {
      const locker = board[i][j];
      let mines = 0;
      if (!locker.isMine) {
        if (i > 0 && board[i - 1][j].isMine) mines += 1;
        if (i < (board.length - 1) && board[i + 1][j].isMine) mines += 1;
        if (j > 0 && board[i][j - 1].isMine) mines += 1;
        if (j < (board[i].length - 1) && board[i][j + 1].isMine) mines += 1;
        if (i > 0 && j > 0 && board[i - 1][j - 1].isMine) mines += 1;
        if (i < (board.length - 1) && j > 0 && board[i + 1][j - 1].isMine) mines += 1;
        if (i > 0 && j < (board[i].length - 1) && board[i - 1][j + 1].isMine) mines += 1;
        if (i < (board.length - 1) && j < (board[i].length - 1) && board[i + 1][j + 1].isMine) mines += 1;
        locker.minesAround = mines;
      }
    }
  }
  return board;
};

export const drawTable = (minePercentage, columns, rows) => {
  const board = [];
  const minesToPlant = Math.floor(columns * rows * (minePercentage / 100));
  let minesPlanted = 0;
  for (let i = 0; i < rows; i += 1) {
    board.push(_.range(columns).map((val, index) => {
      return {
        isMine: false,
        minesAround: 0,
        positionY: i,
        positionX: index,
        key: i * columns + index,
        opened: false,
        flagged: false,
      };
    }));
  }
  while (minesPlanted < minesToPlant) {
    const j = getRandomPosition(columns);
    const i = getRandomPosition(rows);
    if (!board[i][j].isMine) {
      board[i][j].isMine = true;
      minesPlanted += 1;
    }
  }
  return {
    board: getMinesAround(board), minesPlanted,
  };
};

export const changeLockerInfo = (board, positionX, positionY, field, value) => {
  return [
    ...board.slice(0, positionY),
    [
      ...board[positionY].slice(0, positionX),
      {
        ...board[positionY][positionX],
        [field]: value,
      },
      ...board[positionY].slice(positionX + 1),
    ],
    ...board.slice(positionY + 1),
  ];
};
