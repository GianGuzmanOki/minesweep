import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { openLocker, flagLocker, changeGameState, resetState } from '../actions';

const LockerContainer = styled.div`
  width: 40px;
  height: 40px;
  float: left;
  -webkit-box-shadow:inset 0px 0px 0px 1px black;
  -moz-box-shadow:inset 0px 0px 0px 1px black;
  box-shadow:inset 0px 0px 0px 1px black;
  background-color: ${props => props.backgroundColor};
`;

const propTypes = {
  bElem: PropTypes.object.isRequired,
  changeOpenState: PropTypes.func.isRequired,
  changeFlagState: PropTypes.func.isRequired,
};

const Locker = ({
  bElem,
  changeOpenState,
  changeFlagState,
}) =>
  bElem.opened ? (<LockerContainer
    backgroundColor="white"
  >
    {bElem.isMine ? 'M' : bElem.minesAround}
  </LockerContainer>) : bElem.flagged ? (<LockerContainer
    onContextMenu={e => changeFlagState(e, bElem.positionX, bElem.positionY)}
    backgroundColor="green"
  >
    'F'
  </LockerContainer>) : (<LockerContainer
    onClick={() => changeOpenState(bElem.positionX, bElem.positionY, bElem.isMine)}
    onContextMenu={e => changeFlagState(e, bElem.positionX, bElem.positionY)}
    backgroundColor="blue"
  >
    ''
  </LockerContainer>);

Locker.propTypes = propTypes;

const mapStateToProps = null;

const mapDipspatchToProps = dispatch => ({
  changeOpenState: (lockerPosX, lockerPosY, isMine) => {
    dispatch(openLocker(Number(lockerPosX), Number(lockerPosY)));
    if (isMine) {
      dispatch(changeGameState('lost'));
      dispatch(resetState());
    };
  },
  changeFlagState: (e, lockerPosX, lockerPosY) => {
    e.preventDefault();
    dispatch(flagLocker(Number(lockerPosX), Number(lockerPosY)));
  },
});

export default connect(
  mapStateToProps,
  mapDipspatchToProps,
)(Locker);
