export const SET_COLUMN = 'SET_COLUMN';
export const SET_ROW = 'SET_ROW';
export const SET_LEVEL = 'SET_LEVEL';
export const SET_GAME_STATE = 'SET_GAME_STATE';
export const OPEN_LOCKER = 'OPEN_LOCKER';
export const FLAG_LOCKER = 'FLAG_LOCKER';
export const RESET_STATE = 'RESET_STATE';
