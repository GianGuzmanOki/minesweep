import { Record } from 'immutable';
import * as types from '../actions/types';
import { drawTable, changeLockerInfo } from '../utils/helpers';

const levels = {
  easy: 20,
  medium: 40,
  hard: 60,
};

const InitialState = new Record({
  gameState: 'initial',
  level: 'easy',
  columns: 0,
  rows: 0,
  board: [],
  minesPlanted: 0,
  flagsPlanted: 0,
});

export default function (state = new InitialState(), action) {
  switch (action.type) {
    case types.SET_LEVEL:
      return state.set('level', action.value);
    case types.SET_COLUMN:
      return state.set('columns', action.value);
    case types.SET_ROW:
      return state.set('rows', action.value);
    case types.SET_GAME_STATE: {
      const {
        board,
        minesPlanted,
      } = drawTable(levels[state.get('level')], state.get('columns'), state.get('rows'));

      return (
        state
          .set('board', board)
          .set('gameState', action.value)
          .set('minesPlanted', minesPlanted)
      );
    }
    case types.OPEN_LOCKER:
      return state.set('board', changeLockerInfo(state.get('board'), action.positionX, action.positionY, 'opened', !state.board[action.positionY][action.positionX].opened));
    case types.FLAG_LOCKER: {
      const flagBoolean = !state.board[action.positionY][action.positionX].flagged;
      return (
        state
          .set('board', changeLockerInfo(state.get('board'), action.positionX, action.positionY, 'flagged', flagBoolean))
          .set('flagsPlanted', flagBoolean ? state.get('flagsPlanted') + 1 : state.get('flagsPlanted') - 1)
      );
    }
    case types.RESET_STATE: {
      return (
        state
          .set('board', [])
          .set('level', 'easy')
          .set('columns', 0)
          .set('rows', 0)
          .set('minesPlanted', 0)
          .set('flagsPlanted', 0)
      );
    }
    default:
      return state;
  }
}
